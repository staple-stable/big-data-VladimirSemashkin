package task4;

import task2.mappers.BidMapper;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 18.07.2016.
 */
public class ImpMapperTests {
    MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;

    @Before
    public void setUp() {
        BidMapper mapper = new BidMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new LongWritable(1), new Text(
                "8f50f880444aa39dff8afd286229de8c\t20130311000101091\t1\tb7120570f1868f55505ba61a1b943ab2\tMozilla/5.0 (Windows NT 6.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1\t222.36.212.*\t2\t2\t2\ttrqRTuM7Gqq7adj4JKTI\t83ecb0982b64877311b600d8d037321\tnull\t2318061531\t250\t250\t2\t0\t5\t3a0cf3767556609a1f4329c9f52e387e\t300\t33\t9f4e2f16b6873a7eb504df6f61b24044\n"));
        mapDriver.withOutput(new Text("b7120570f1868f55505ba61a1b943ab2"), new IntWritable(1));
        mapDriver.runTest();
    }
}