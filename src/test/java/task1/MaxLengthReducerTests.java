package task1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import task2.mappers.BidMapper;
import task2.reducers.BidReducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Semashkin Vladimir on 23.07.2016.
 */
public class MaxLengthReducerTests  {
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;

    @Before
    public void setUp() {
        BidReducer reducer = new BidReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testReducer() throws IOException {
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(3));
        reduceDriver.withInput(new Text("abc"), values);
        reduceDriver.withOutput(new Text("abc"), new IntWritable(3));
        reduceDriver.runTest();
    }
}
