package task3.sequence.output;

import org.apache.hadoop.mapreduce.Counter;
import org.junit.Assert;
import task3.mappers.IpMapper;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpMapperTests  {
    MapDriver<LongWritable, Text, Text, LongWritable> mapDriver;

    @Before
    public void setUp() {
        IpMapper mapper = new IpMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void should_process_row() throws IOException {
        mapDriver.withInput(new LongWritable(1), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapDriver.withOutput(new Text("ip1"), new LongWritable(40028));
        mapDriver.runTest();
        for (Counter cnt : mapDriver.getCounters().getGroup(IpMapper.USER_AGENT_COUNTER_GROUP_NAME)){
            System.out.println(cnt.getDisplayName() + " = " + cnt.getValue());
            Assert.assertEquals("should be equal",cnt.getDisplayName() + " = " + cnt.getValue(), "Mozilla = 1");
        }
    }
}