package task3.nice.csv;

import task3.combiners.IpCombiner;
import task3.mappers.IpMapper;
import task3.reducers.IpReducerWithNiceCsv;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpMapReduceWithNiceCsvTests {
    MapReduceDriver<LongWritable, Text, Text, LongWritable, Text, Text> mapReduceDriver;

    @Before
    public void setUp() {
        IpMapper mapper = new IpMapper();
        IpReducerWithNiceCsv reducer = new IpReducerWithNiceCsv();
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void should_process_row() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("ip1"), new Text("40028,40028"));
        mapReduceDriver.runTest();
    }

    @Test
    public void should_process_row_with_combiner() throws IOException {
        mapReduceDriver.setCombiner(new IpCombiner());
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("ip1"), new Text("40028,40028"));
        mapReduceDriver.runTest();
    }

    @Test
    public void shouldProcessRowWhenThererIsNoBites() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 ThereIsNoBites \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 123 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("ip1"), new Text("123,123"));
        mapReduceDriver.runTest();
    }

    @Test
    public void shouldProcessRowWhenThererIsNoIp() throws IOException {
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ThereIsNoI_P12 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withInput(new LongWritable(), new Text(
                "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 123 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""));
        mapReduceDriver.withOutput(new Text("ip1"), new Text("123,123"));
        mapReduceDriver.runTest();
    }
}
