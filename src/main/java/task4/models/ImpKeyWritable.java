package task4.models;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 23.07.2016.
 */
public class ImpKeyWritable implements WritableComparable<ImpKeyWritable>
{
    private Text city;
    private Text userAgent;

    public ImpKeyWritable()
    {
        city = new Text();
        userAgent = new Text();
    }

    public void set(String city, String userAgent)
    {
        this.city.set(city);
        this.userAgent.set(userAgent);
    }

    public Text getCity()
    {
        return city;
    }

    public Text getUserAgent()
    {
        return userAgent;
    }

    public void readFields(DataInput in) throws IOException
    {
        city.readFields(in);
        userAgent.readFields(in);
    }

    public void write(DataOutput out) throws IOException
    {
        city.write(out);
        userAgent.write(out);
    }

    public int compareTo(ImpKeyWritable o)
    {
       return (city.compareTo(o.city));
    }

    @Override
    public boolean equals(Object o)
    {
        if (o instanceof ImpKeyWritable)
        {
            ImpKeyWritable other = (ImpKeyWritable) o;
            return city.equals(other.city);
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return city.hashCode();
    }
}
