package task1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 16.07.2016.
 */

public class MaxLengthReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
    private final static IntWritable maxLength = new IntWritable(0);
    private static Text keyWithMaxLength = new Text("none");

    @Override
    public void reduce(Text key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {
        for (IntWritable val : values) {
            //if many words have the same length, it depends on < or <= what word will be eventually written into the context
            if (maxLength.get() < val.get()){
                maxLength.set(val.get());
                keyWithMaxLength.set(key);
            }
        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        context.write(keyWithMaxLength, maxLength);
            /*super.cleanup(context);*/
    }
}