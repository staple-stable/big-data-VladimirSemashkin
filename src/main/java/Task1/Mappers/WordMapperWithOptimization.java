package task1.mappers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Created by Semashkin Vladimir on 16.07.2016.
 */

public class WordMapperWithOptimization extends Mapper<LongWritable, Text, Text, IntWritable> {
    private final static IntWritable length = new IntWritable(0);
    private Text word = new Text();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        if (value != null) {
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            //if many words have the same length, it depends on < or <= what word will be eventually written into the context
            while (tokenizer.hasMoreTokens()) {
                // let's return to the reducer only the longest words
                String currentWord = tokenizer.nextToken();
                if (length.get() <= currentWord.length()) {
                    String token = currentWord;
                    word.set(token);
                    length.set(token.length());
                    context.write(word, length);
                }
            }
        }
    }
}