package task3;

import common.ConfigurationProvider;
import task3.combiners.IpCombiner;
import task3.mappers.IpMapper;
import task3.reducers.IpReducerWithNiceCsv;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpAnalyzerWithNiceCsv  extends Configured implements Tool {
    public static ConfigurationProvider configurationProvider = new ConfigurationProvider();

    public int run(String[] args) throws Exception {

        Job job = Job.getInstance(getConf()); // new approach
        // to take on fly properties and configurations
        job.setJobName("ip job with nice csv");
        /*
        To distribute code across nodes (not main class only)
         */
        job.setJarByClass(IpAnalyzerWithNiceCsv.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setMapperClass(IpMapper.class);
        job.setCombinerClass(IpCombiner.class);
        job.setReducerClass(IpReducerWithNiceCsv.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        configurationProvider.setOutputSeparator(",");
        int res = ToolRunner.run(configurationProvider.get(), new IpAnalyzerWithNiceCsv(), args);
        System.exit(res);
    }

}