package task3;

import common.ConfigurationProvider;
import task3.mappers.UserAgentMapper;
import task3.reducers.UserAgentReducer;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class UserAgentAnalyzer extends Configured implements Tool {
    public static ConfigurationProvider configurationProvider = new ConfigurationProvider();

    public int run(String[] args) throws Exception {

        Job job = Job.getInstance(getConf()); // new approach
        // to take on fly properties and configurations
        job.setJobName("user agent job");
        /*
        To distribute code across nodes (not main class only)
         */
        job.setJarByClass(UserAgentAnalyzer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setMapperClass(UserAgentMapper.class);
        job.setReducerClass(UserAgentReducer.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(configurationProvider.get(), new UserAgentAnalyzer(), args);
        System.exit(res);
    }


}
