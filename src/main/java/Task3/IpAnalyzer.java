package task3;

import common.ConfigurationProvider;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Counter;
import task3.combiners.IpCombiner;
import task3.mappers.IpMapper;
import task3.model.IpWritableModel;
import task3.reducers.IpReducer;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpAnalyzer  extends Configured implements Tool {
    public static ConfigurationProvider configurationProvider = new ConfigurationProvider();

    public int run(String[] args) throws Exception {

        JobConf conf = new JobConf(getConf(), IpAnalyzer.class);
        conf.setJobName(this.getClass().getName());
        Job job = Job.getInstance(getConf()); // new approach
        // to take on fly properties and configurations
        job.setJobName("ip job");
        /*
        To distribute code across nodes (not main class only)
         */
        job.setJarByClass(IpAnalyzer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IpWritableModel.class);

        job.setMapperClass(IpMapper.class);
        job.setCombinerClass(IpCombiner.class);
        job.setReducerClass(IpReducer.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        int result = job.waitForCompletion(true) ? 0 : 1;

        for (Counter cnt : job.getCounters().getGroup(IpMapper.USER_AGENT_COUNTER_GROUP_NAME)){
            System.out.println(cnt.getDisplayName() + " = " + cnt.getValue());
        }

        return result;
    }

    public static void main(String[] args) throws Exception {
        configurationProvider.setProperty("mapreduce.output.fileoutputformat.compress", "true");
        configurationProvider.setProperty("mapreduce.output.fileoutputformat.compress.codec", "org.apache.hadoop.io.compress.SnappyCodec");
        configurationProvider.setProperty("mapreduce.output.fileoutputformat.compress.type", "BLOCK");
        int res = ToolRunner.run(configurationProvider.get(), new IpAnalyzer(), args);
        System.exit(res);
    }


}
