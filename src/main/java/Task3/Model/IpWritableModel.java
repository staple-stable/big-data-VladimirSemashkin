package task3.model;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */

public class IpWritableModel implements Writable,Serializable {
    private LongWritable Avg = new LongWritable(0);
    private LongWritable Total = new LongWritable(0);

    public IpWritableModel(){

    }

    public IpWritableModel(long avg, long total){
        Avg.set(avg);
        Total.set(total);
    }

    public void write(DataOutput out) throws IOException {
        Avg.write(out);
        Total.write(out);
    }

    public void readFields(DataInput in) throws IOException {
        Avg.readFields(in);
        Total.readFields(in);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof IpWritableModel){
            IpWritableModel model = (IpWritableModel)obj;
            return model.Total.equals(Total) && model.Avg.equals(Avg);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Avg.hashCode() + Total.hashCode();
    }

    @Override
    public String toString() {
        return this.Avg.toString() + "\t" + this.Total.toString();
    }
}