package task3.reducers;

import task3.model.IpWritableModel;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpReducer  extends Reducer<Text, LongWritable, Text, IpWritableModel> {

    @Override
    public void reduce(Text key, Iterable<LongWritable> values, Context context)
            throws IOException, InterruptedException {
        long sum = 0;
        Integer cnt = 0;
        for (LongWritable val : values) {
            sum+=val.get();
            cnt++;
        }
        IpWritableModel ipModel = new IpWritableModel(sum/cnt,sum);
        context.write(key, ipModel);
    }
}
