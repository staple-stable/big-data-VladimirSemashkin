package task3.mappers;

import task3.services.IpInfoService;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class IpMapper extends Mapper<LongWritable, Text, Text, LongWritable> {
    public final static String USER_AGENT_COUNTER_GROUP_NAME = "User-Agents";
    private final static LongWritable bites = new LongWritable(1);
    private final IpInfoService ipInfoService = new IpInfoService();
    private final static Text ip = new Text();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        context.setStatus("mapping...");
        super.setup(context);
    }

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        if (value != null){
            String ipValue = ipInfoService.getIp(value);
            Long bitesValue = ipInfoService.getBites(value);
            if (bitesValue != null && ipValue != null){
                setUserAgentCounter(value,context);
                ip.set(ipValue);
                bites.set(bitesValue);
                context.write(ip, bites);
            }
        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        context.setStatus("mapping phase has finished...");
        super.cleanup(context);
    }

    private void setUserAgentCounter(Text value, Context context){
        String userAgentValue = ipInfoService.getUserAgent(value);
        if (userAgentValue != null){
            context.getCounter(USER_AGENT_COUNTER_GROUP_NAME, userAgentValue).increment(1);
        }
    }
}

