package task3.mappers;

import task3.services.IpInfoService;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by Semashkin Vladimir on 20.07.2016.
 */
public class UserAgentMapper extends Mapper<LongWritable, Text, Text, Text> {
    private final static LongWritable one = new LongWritable(1);
    private final IpInfoService ipInfoService = new IpInfoService();
    private final static Text ip = new Text();
    private final static Text userAgent = new Text();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        if (value != null){
            String ipValue = ipInfoService.getIp(value);
            String userAgentValue = ipInfoService.getUserAgent(value);
            if (userAgentValue != null && ipValue != null){
                ip.set(ipValue);
                userAgent.set(userAgentValue);
                context.write(userAgent, ip);
            }
        }
    }
}

